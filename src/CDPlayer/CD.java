package CDPlayer;

import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Daniel
 */
public class CD {
    
    private ArrayList<Song> songList;
    private String CDName;
    
    public CD(Song s1,Song s2,Song s3,Song s4,String name){
        songList = new ArrayList<>();
        songList.add(s1);
        songList.add(s2);
        songList.add(s3);
        songList.add(s4);
        this. CDName = name;
        
    }
    
    public String getSong(int tracknum){
        
        return songList.get(tracknum).getSongName();
        
    }
    
    public String getCDName(){
        
        return this.CDName;
        
    }
    
    public Song getSongObj(int tracknum){
        return songList.get(tracknum);
        
    }
}
