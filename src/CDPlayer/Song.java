package CDPlayer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Daniel
 */
public class Song {
    
    private int songDuration;
    private String songName;
    
    public Song (int durationms,String name){
        
        this.songDuration = durationms; // in milliseconds, 5 min = 300 000 ms
        this.songName = name;
        
    }
    
    public String getSongName(){
        return this.songName;
    }
    
    public int getDuratino(){
        return this.songDuration;        
    }
    
    
}
