/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures;

/**
 *
 * @author Daniel
 */
public class CD implements Comparable<CD>{
    
    String cdName;
    
    public CD(double s){
        this.cdName = "seedee"+s;
    }
    
    public String cdName(){
        return this.cdName;
    }
    public int compareTo(CD other){
        int last = this.cdName.compareTo(other.cdName);
        return last;
    }
}