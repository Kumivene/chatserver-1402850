/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coffeemachine;

import java.util.Scanner;

/**
 *
 * @author Daniel
 */
public class User {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        CoffeeMachine kahvikone = new CoffeeMachine();
        String input = "default";
        Scanner reader = new Scanner(System.in);
        

        while(!input.equals("exit")){
            
            System.out.println("\nSelect a function:\n1.On/Off\n2.Clean Machine\n3.Black Coffee\n4.Espresso\n5.Fill with beans\n6.Fill with water\n7.Check Machine status\nType \"exit\" to quit\n");
            input = reader.nextLine();
            
            switch(input){
                
                case "1":   kahvikone.toggleOnOff();
                            break;
                case "2":   kahvikone.cleanMachine();
                            break;
                case "3":   kahvikone.blackcoffee();
                            break;
                case "4":   kahvikone.espresso();
                            break;
                case "5":   kahvikone.fillBeans();
                            break;
                case "6":   kahvikone.fillWater();
                            break;
                case "7":   kahvikone.checkStatus();
                            break;
                case "exit":System.out.println("Bye Bye!");
                            break;
                default:    throw new IllegalArgumentException("Error!");
            }
            
        }
        
    }
    
}
