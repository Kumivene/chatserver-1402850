/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package waterdispenser;

/**
 *
 * @author Daniel
 */
public class WaterDispenser {

    private int waterLevel; //maximum 10 000 ml
    private boolean powerState; //false = off -- true = on
    private boolean lidState; //false = closed -- true == open
    private int cup; //default cup size = 100 ml
    
    
    public  WaterDispenser() {
    
        this.waterLevel = 10000;
        this.lidState = false;
        this.powerState = false;
        this.cup = 100;
    
    
    }
    
    
    public void waterButton(){
        if (this.powerState){
            if (waterLevel >= cup){

                this.waterLevel = this.waterLevel - 100;
                System.out.println("Enjoy your cup of water!");

            }
            else{
                System.out.println("Not enough water!");
            }
        }
        else {}
    }
    
    
    public void toggleLid(){
        this.lidState = !this.lidState;
    }
    
    
    public void fillWater(){
        
        if(lidState){
            
            this.waterLevel = 10000;
            System.out.println("Watertank full.");
            
        }
        else{
            System.out.println("Open lid first!");
        }
    }


    public void powerToggle(){
    
    this.powerState = !this.powerState;
    System.out.println("Beep!");
    
}
}
